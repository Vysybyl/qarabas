---
title: "Melancholic layer"
date: 2021-08-30T22:03:13+02:00
draft: false
featured_image: '/images/2021-08-31_vetralla.jpg'
---

*Infinite love,   
great tenderness  
and gentle care  
are in each leaf of grass.*

*While we walk absent-minded  
carrying on with our business  
or creating emptiness  
plowing, crying,  
peeling the love off our next daisy,  
dusting crumbles and ancient vases  
for the next meal;  
wheat grows and blossoms  
grapes swallen and fall  
corn... corn... corn...*  

(\- Saskia, is there a way to say "turns into dust" with one single word?  
\- ...cremates)  

*Then...  
corn cremates into bones.  
Blood flows, not just through the ears  
and toes and veins  
but in creeks and streams on the ground  
up the bark of the pine trees  
through insects, birds, women and big mammals,  
and rains in fly-shaped drops  
onto rocks and stones*  

*With majestic sadness, 
but no real grudge,  
the last Rhino is born  
"I shall go too" - he blows meekly through his horn.  
The last Tazmanian Wolf  
barks or coffs or howls  
her last vowel.  
A forest is burnt  
a leaf of grass grows*

*With infinite love,   
great tenderness  
and gentle care  
two leaves of grass have grown  
so that I could remember  
\- standing on the top of my head -  
the 23rd verse  
of an old song.*
