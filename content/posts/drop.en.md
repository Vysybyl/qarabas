---
title: "Drop"
date: 2021-08-28T22:03:13+02:00
draft: false
featured_image: '/images/2021-08-28_rain_yurt.jpg'
---

*Yuppi-dee-doo  
no tie no shoes  
drops on the feet  
hands on knees*  

*Drippi-dee drop  
we haven't stopped  
to wash, to stream,  
to flow, to clean*

*Washa-ti woosh  
no pull no push  
the eye, the brows,  
the dogs, the crows*  

*Ow-ow-oww!  
the sky is blue  
the floor is brown  
I'm on the ground*
