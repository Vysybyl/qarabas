---
title: "Lettera da oltre il confine"
date: 2022-03-30T21:55:13+02:00
draft: false
featured_image: '/images/2021-08-28_rain_yurt.jpg'
---

Caro Babbo,

Ti prego perdonami del lungo tempo che ho lasciato passare dall'ultima lettera che mi mandasti. Come sai, il servizio postale oltre confine non funziona bene e la cancelleria è razionata.

Spero che tu stia bene e che quella noia al ginocchio di cui mi scrivesti sia passata. Come procedono le faccende all'azienda? Lavori ancora tante ore? Ti prefo, se non l'hai ancora fatto, di prenderti un aiuto ora che la cara mamma non c'è più. Anzi, ti darò già una buona notizia, babbino caro: quest'aiuto potrai pagarlo pure con la mia mensilità, giacché da questo mese non ne ho più bisogno! Ho infatti finalmente trovato un lavoro; quale esso sia, te lo racconterò più avanti. Ne sono molto contento e orgoglioso: credo che, di tutte le busse che mi desti per il mio bene per raddrizzarmi, infine mi sono messo dritto.

Io sto bene di fisico e mentalmente. Ti scrivo questa missiva innanzitutto per rassicurarti di come vanno le cose in questo Paese, che certo è diverso dal nostro ma che, come vedrai, non lo è affatto così tanto.

Innanzitutto vi è la faccenda del latte. Io ho sentito di certe storiacce che pubblicano i giornali a casa, e volevo raccontarti invece di quello che ho visto e saputo, dimodoché tu - ricevendo notizie vere e di prima mano - non ti lasci ingannare.

Innanzitutto c'è da dire una cosa: bere latte qui non è obbligatorio. Questa è una prima menzogna che va denunciata. Certo, bisogna dire, non è obbligatorio ma è *fortemente raccomandato*, a causa della crisi del latte.

Di questa crisi tu certo devi già aver sentito parlare, ma lascia che ti rispieghi io per dritto come stanno le cose.

Come tutti sanno, al centro di questo paese vi è l'economia della carne. Tutto il resto funziona attorno a quella, e se quella si ferma, non vi sono denari per il resto: i teatri, gli ospedali, le scuole, le chiese e i cimiteri, eccetera. Per questa ragione, il paese è all'avanguardia su tutto quello che riguarda l'allevamento e le bestie: i mangimi, le razze, le tecniche di macellazione, eccetera.

Qualche anno fa, credo più di cinque ma meno di venti, tutta questa scienza delle bestie è arrivata all'apice con la creazione della razza Albertina. Si tratta di una vacca tirata su non so bene come - mezza naturale, mezza artificiale - che cresce più in fretta, produce più latte, partorisce più vitelli di qualsiasi altra. Per facilitare la produzione, gli scienziati delle vacche di questo paese l'hanno pure resa più sveglia: così la bestia capisce quand'è l'ora della mungitura e si mette lì pronta alle macchine, non s'impiglia la testa negli steccati, non s'arrischia di cadere quando pascola in montagna, al macello s'allinea buona buona con le altre, eccetera. Io credo però che gli scienziati vaccini abbiano un poco esagerato con l'intelligenza dell'Albertina, perché questa non solo è docile e si accomoda bene ad essere allevata e scannata, ma c'ha pure una specie di intuito affinatissimo e un fiuto da cane da tartufi.

E qui è successo il guaio: le Albertine non sopportano l'odore di chi non beve latte. 
All'inizio della crisi le vacche fiutavano il mandriano e, se sentivano che questi non aveva bevuto latte nell'ultimo mese, s'incattivivano e s'imbizzarrivano. Parecchi ne mandarono al pronto soccorso prima che gli scienziati vaccari capissero cosa stava accadendo.


?????

Io, come tu sai, il latto lo adoro perché mi sono abituato a berne sempre e tanto da piccolo in aziendo, e quando ho sentito che ne davano gratis non stavo più nella pelle ed ero sempre tra i primi della fila la domenica. 

Dopo forse due mesi però ci si rese conto che i calcoli fatti all'inizio avevano qualcosa di sbagliato. Quali che siano questi calcoli io non lo so, perché non sono pubblici e c'è un consesso speciale di esperti di vacche che se ne occupa, ma io credo che loro si aspettassero che il latte gratis lo volessero tutti, e invece già io potevo dire loro che di venti compagni di scuola ce n'erano due a cui non piaceva e uno che proprio era intollerante...
Certo so che loro non hanno mai detto "se tre quarti della popolazione beve un bicchiere alla settimana la crisi rientra" ma certo tra i fogli di questi esperti vaccari questo numero ci doveva essere. Comunque, devono aver rifatto i conti e han deciso che, insomma, bisognava dare qualche incentivo, qualche raccomandazione.

????
Al cinema, se bevi un bicchiere di latte all'ingresso il costo del biglietto si dimezza. Al ristorante che beve latte non paga da bere.

???
Finché a Novembre non sono nate le Carlone. 



