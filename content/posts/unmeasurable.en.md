---
title: "Talking boxes - A thought experiment on unmeasurable traits of two complex systems in interaction"
date: 2022-07-12T19:15:52+02:00
draft: false
featured_image: '/images/2022-07-04_Chiesa_teatro.jpg'
---


## INTRODUCTION

This is a philosophical contribution on all studies on artificial (general) intelligence, the neurology of consciousness and of free will, and hopefully on the political and public debates in general. Rather than presenting a new theory or supporting an existing one, it challenges their groundedness by demonstrating their fundamentally aporetical (paradoxical) structure with a thought experiment. This thought experiment can be adapted with minor modifications to challenge any metaphysical claims on unmeasurable traits that may arise from within reaserch environments or other collective exercises.


## UNMEASURABLE TRAITS

This play may be performed by different characters. I might not know all of their names but I can tell how their actions will unfold, since their role is strict and simple.

In lack of better words, I am calling them *unmeasurable traits of complex systems*. This category includes but is not limited to: life, intelligence, artistical value, economical value, political power, consciousness and free will. If they were still fashionable, I would gladly include god and the soul in their number, but that story has already been told.

Unmeasurable complexity shall be included among these traits, while measurable complexity (even relative measurable complexity) as the one defined in assembly theory does not belong here.

Now, I feel forced to give a definition of what an unmeasurable trait is. While knowing that it will cause much resistance in the reader, I will define unmeasurable those traits on which no attempted measure has reached a common agreement. Obviously I am not talking about different units of measure, but rather a different order: given two complex systems A and B and two individuals X and Y, individual X may find that "system A has more trait then system B", while individual Y thinks that "system B has more trait then system A".


## STATEMENT

**Any theory that claims to obtain a measure on an unmeasurable trait of a complex system based on the observation of its interaction with another systems is undecidable**


## SETUP

**Two systems are interacting and at least one of them has an unmeasurable trait.**

First of all, we must have two distinct systems. They must be individuable and distinguishable (there must be an agreement on where one system ends and the second one starts). If the criticized theory presents a set of more then two systems, it will be sufficient to group all of them but one in one system and to identify the remaining one as the second system.
 
The two systems are interacting: they must be exchanging information, energy or other measurable quantities. The interaction must be looped (bidirectional). This is not an important restriction since all real-life cases of interaction are bidirectional and no interesting case of monodirectional interaction that foster metaphysical claims exists.

At least one of the systems must have an unmeasurable trait. Since the setup is symmetric, it does not matter which one. 

**The criticized theory aims to obtain a test or measure of the unmeasurable trait in one of the systems by observing or measuring the interaction.**

This setup is all that is needed for the unmeasurable trait to be undecidable.


## CONSEQUENCES

### LOST OF LOCALITY

**Unmeasurable traits of interacting systems spread.** 

If the criticized theory admits the setup described above, the unmeasurable trait will lose a defined location and "spread" to the entire coupled system. If the two systems were originally labelled with different levels of the traits, the trait will spread evenly among them.

If we consider the trait segregated to the first system, it means that the second system will be driving the interaction: no measure of the first system trait can therefore be inferred by observing the interaction.


### LOST OF CAUSALITY

**Unmeasurable traits in interacting systems enter a causality loop.**

If the interaction is bidirectional, the unmeasurable trait will be influenced by the feedback from the other system. Now, if we consider the first system to be the first agent of the interaction, it means that the second system will be driving the interaction: it will have more power than the first one. In this case we can swop them but we will still reach the same setup. Both systems are causing each others at the same time.


### LOST OF DECIDABILITY

**Due to lost of locality and lost of causality, the interactions between the two systems can not be used as a test for the criticized theory. The theory is undecidable.**

Whatever test or measure is taken on the interaction, it may never be used to measure the trait on *one* of the systems.


### SOME EXAMPLES

A dog and the owner. (Will, Intelligence, Life, Power, Consciousness) 
A dominant class and a dominated class. (Will, Power - this is a traditional example from Hegel)
A reasearcher and an artificial intelligence. (Will, Intelligence, Life, Consciousness - This is a modern example)
A piece of art and its maker. (Artistic Value, Will)
Two lovers. (Love - this is a traditional example from St. Augustine)
A worker and their tool or their task. (Economic Value, Will)
A book and its reader. (Will, Intelligence, Consciousness)
A live performer and their audience. (Will)
A sport team and its supporters. (Economic Value, Will)
God and the creation. (Will, Intelligence, Life, Power, Beauty, Consciousness, ...)
Mankind and the universe. (Will, Intelligence, Life, Power, Beauty, Consciousness, ...) 


## PARTIAL SOLUTIONS

The aporia does not affect the *existence* of the unmeasurable trait. It only invalidates the possibility to measure its amount in a separated system by measuring that system interaction with another one. 

Two traditional solutions to this situation come at hand. The first one (that we may call *atheist*) implies renouncing the existance of the trait: god, consciousness, free will, etc. do not exist. The second one (that can be called *pantheistic*) entails renouncing the locality of the trait: god, consciousness, free will, etc. are everywhere (and everytime).


## NOTES ON UNMEASURABILITY

Unmesaurability (thus unmeasurable traits) is a byproduct of the principle of individuation. In other words, it is a byproduct of our language.

Unmeasurable traits are trascendentals.

Unmeasurable traits have the same status as Croce's "concepts" (*concetti*). In Croce's terms we might say that the undecidable metaphysical claims in the criticized theory arise from mixing concepts and pseudoconcepts (*concetti* and *pseudoconcetti*).

Wittgenstein might say that the aporia arise from mixing the *game of measuring* with the *game of persuading*.

