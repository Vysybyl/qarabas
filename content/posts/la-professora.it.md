---
title: "La Professora"
date: 2021-11-09T12:47:38+01:00
draft: false
featured_image: '/images/2019-02-09_mare_inverno.jpg'
---

*(canzone)*

*Il mare.  
In ogni bar scassato un cane, una chitarra  
un ragazzo e un bancone  
con una spina di birra*

*Camminavo con lei ragionando,  
se volevo dieci, venticinque o cinquanta  
se con dieci si sarebbe offesa  
e come mi avrebbe toccato in auto  
o a casa*

*Passeggiava  
come una sciura in via Roma  
stretta al mio braccio,  
sulla strada lurida tra il fiume d'inverno  
e le baracche tirate su a casaccio*

*Signora mia, Maria  
\- la chiamavo -  
e sentivo il sua amore infinito di mamma  
ancora più forte e grande perché grassa e vecchia  
\- lei sapeva*

*E io, che mi ero detto "Finalmente!"  
scoprendo al suo fianco quella strada di Lisbona  
o Cova do Vapor, da Moura, da Piedade,  
che era rimasta intatta per non esserci mai stata*  

*Me n'ero innamorato,  
diventando scrittore nonostante la paura boia che porta  
Sarei tornato là mille volte a scrivere in ogni baracca,  
guardato male dai vecchi   
e con curiosità dai ragazzi  
mentre raccontavo le loro storie di gente morta  
o mai nata*

*"È tornata Maria! È tornata uguale!"  
i ragazzi e i vecchi escono e la salutano  
"professora"   
e io so che anche se le offrirò dieci solo per parlare,  
lei mi toccherà uguale*  

*Poveri voi che mi guardate male  
e non sapete che l'amore  
è una vecchia grassa "professora" de Lisboa  
che vende qualche favore*  
