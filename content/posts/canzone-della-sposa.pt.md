---
title: "Canzone della sposa"
date: 2022-02-26T17:47:38+01:00
draft: false
featured_image: '/images/2021-02-11_zogno.jpg'
---

*(canzone)*

*Si sposa tuo figlio, ti han detto  
che bisogna addobbare questo vecchio tronco,  
rifarlo a festa come quand'era Maggio  
e uscivi sola dal letto disfatto.*  

*La processione di maschere ed animali  
scende il colle dietro al cimitero  
tintinnano i pendagli della sposa senza vera  
battono il terreno le uniformi e gli stivali*  

*Il paese ti guarda stupito  
fa "oh" sorpreso della bomba sbocciata  
in pieno seno, in tarda sera,  
in pieno centro, centrato in pieno,  
pieno di gente sorpresa  
stupita dal brillare delle mente*  

*e la staccionata su cui eri salita  
per baciare in alto Piotr prima che Sasha ti portasse via  
è stanca di essere scavalcata, si sdraia a terra  
come la vite greca, come Lyosha e la zia.*

*Datemi un sorriso per mio figlio che si sposa  
l'abito красивое ce l'ho da Maggio,  
è arrivato da ovest,  
è arrivato in aereo,  
lo strascico lo fanno tende bianche кухня, alla finestra  
c'è un buco da vedere  
e nessuno a guardare.*

*Вода, водка, пиво и вино  
vento caldo spazza la strada  
vino, vodka, birra e acqua  
un giovane uomo si sceglie un destino*

*Rivesti i rami come fosse a Maggio  
i petali prima, i petali bianchi,  
i frutti rossi, gli occhi stanchi.  
Ti han detto "Девочка! Bellezza miraggio!"*

*Dici ad Anja che è bella  
dici al figlio che è pronto  
l'incenso fa fumo l'incendio sul monte  
è scesa una nuvola e prima una stella*
