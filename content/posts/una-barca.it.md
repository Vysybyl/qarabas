---
title: "Una barca"
date: 2021-07-26T00:52:49+02:00
draft: false
featured_image: '/images/2020-08-18_relic.jpg'
---

*metafora*

Siamo su una piccola barca in mezzo all'oceano. Non c'è terra a vista. Il cielo è limpido e la brezza continua ma dolce, eppure le onde ancora gonfie sembrano suggerire che una tempesta è appena passata. Da quanto tempo navighiamo? 

Come in un sogno, né tu né io lo ricordiamo. Ma il rollio della barca, il calore del sole e il vento fresco ci appaiono presenti e reali. C'è un timone e un piccolo albero con una vela triangolare ammainata. Il timone sembra intatto ma, senza punti di riferimento, è impossibile dire se reggerlo serva davvero a mantenere una direzione. Forse, sotto il pelo dell'acqua, sporge solo un moncherino spezzato.

Il sole è alto e comincia a scaldare troppo. A volte, la vela sembra dondolare a sufficienza da spingere la barca; altre ci pare di essere sospinti da una corrente o di non muoverci affatto. Né io né tu riusciamo ad esserne sicuri e, quando uno è certo che sia il vento, l'altra insiste che siamo fermi. Se poi qualche volta si convince, il primo dubita e cambia idea.

Ti dirò di guardare le onde. Ti sembreranno chiare, separate e ordinate: quella lì bianca di schiuma, quell'altra larga e bassa, quella laggiù più scura a causa forse di un riflesso del fondale o di una nuvola...

Mano a mano che si avvicinano, però, sarà più difficile distinguerle: la macchia scura è passata all'onda successiva, e quella che il vento rompeva in schiuma è ora separata in due. La grande ha sostenuto il peso della barca e dopo averci superati non si vede più. Ti dirò: "guarda nuovamente!"

Potremmo ingegnarci per cambiare la nostra rotta. Spiegare la vela, ad esempio, o gettarci in acqua per verificare se il timone è intatto. Senza sapere che direzione prendere però, sarebbe forse utile? L'odore dei pini, che a volte ci pare di sentire, non indica che la terra è vicina? Forse i soccorsi sono prossimi e muovendoci li mancheremmo. Quanto è profondo il mare qui?

Potremmo invece rilassarci. Ci sono ancora delle provviste e il calore del sole si riesce a sopportare se ci mettiamo all'ombra dell'albero. Ci sfideremo con degli indovinelli oppure comporremo canzoni nell'attesa.

La terraferma, i soccorsi, oppure il ricordo, appariranno prima che passi un altro giorno.
