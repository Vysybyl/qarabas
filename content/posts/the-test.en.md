---  
title: "The test"  
date: 2021-12-16T22:13:13+02:00  
draft: false  
featured_image: '/images/2019-07-03_macao.jpg'  
---  
  
*(song)*  
  
*I write in English as a code    
my words sound hollow like the call  
of a distant past,  
no rest, no distress,  
maybe just a small test  
to all those people that know what's best*  
  
*Mummy's soul hanging from a tree,  
but daddy, he does care I have my shoes on my feet,  
they both signed to pay the family fee,  
so that I can too find my way to hang me*  
  
*People come and people go  
like gods, sparrows, cars and breath,  
If you'd only see what I can see  
these heavy beats would fall into their track*  
  
*I am so crazy I don't even scream  
that what you call reality is a constipated dream  
'cause I still think a peaceful conversation  
is better than having to fight a whole nation*  
  
  
*I know what's best,   
I need to rest,  
I need to study for the test,  
I had some dates and I have a good guess*  
  
*When the walls will fall,   
the rope will cling to my throat  
I'll still dance my way around your mess*  
  
*I know what's best,   
I need to rest,  
I need to study for the test,  
I had some dates and I have a good guess*  

*When the walls will fall,   
the rope will cling to my throat  
I will dance my words around your scape-goat*   
  

*I don't seem to have a meaningful conversation  
I don't seem to be able to talk to the population  
I am your example of a working borderline  
I can attach my colors to your brain and you'll feel fine*  
  
*I am lost, but don't ask  
I'm a ghost, I am better then most,  
I am what you cannot even think  
I can see the frozen moment when everyone sees the end of the fork*  
  
*Teach me, I'll be your guide  
I'll be the light that sets the fire in your smile  
I am alone, I'm a mole, I am a four year old  
and I am barren like a toad*  
  
*I am your son, your sin  
Your tidy mind is my worst dream  
I can't keep no uninterrupted attention  
but I swear I have no good intentions*  
  
*My vibe is soft, my name is lost,  
but I'm still better then most,  
your underfunding me is like a cut into your spleen  
your screen is never seen my scream you'll never hear  
but I can still touch my funny nose with my two humble knees*  
  
*I wanna be rich, I wanna be old  
but I'm still better then most  
I am just hanging from a tree  
Climbing back to where you people will not find me*  
  
*I won't be rich, I won't be old  
but I'm still better then most  
I am just hanging from a tree  
Climbing back to where you people will not haunt me*  
