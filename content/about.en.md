---
title: "About"
date: 2021-07-16T01:39:49+02:00
draft: true
featured_image: '/images/2021-07-10_swan_bw_cazzago.jpg'
---
I want a place to store thoughs, a notebook easy to share and a free experimental space.

### The name *Qarabas*

comes from a madman dressed with royal insignia by the people of Alexandria to mock king Herodes Agrippa I, mentioned by Philo in his *Flaccus*. What does a madman treated like a king think?