---
title: "About"
date: 2021-07-16T01:39:49+02:00
draft: false
featured_image: '/images/2021-07-10_swan_bw_cazzago.jpg'
---
Quero um lugar para guardar pensamentos, um bloco de notas simples para partilhar e um espaço livre experimental.

### O nome *Qarabas*

deriva daquele de um louco enfeitado com emblemas reáis pelos habitantes de Alexandria para rebaixar o rei Herodes Agripa I, relatado por Fílon de Alexandria no *Flaccus*. O quê pensa um malouco tratado como um rei?