---
title: "About"
date: 2021-07-16T01:39:49+02:00
draft: false
featured_image: '/images/2021-07-10_swan_bw_cazzago.jpg'
---
voglio un posto dove poter conservare i pensieri, un quaderno per appunti facile da condividere e uno spazio libero sperimentale


### Il nome *Qarabas*

deriva da quello di un matto adornato con insegne regali dagli abitanti di Alessandria d'Egitto per schernire il re Erode Agrippa I, menzionato da Filone d'Alessandria nel *Flaccus*. Cosa pensa un matto trattato come un re?

